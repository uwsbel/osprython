import sys
import ospray
import ospray.utils
from ospray.constants.types import *
from ospray.constants.formats import *
from ospray.constants.channels import *

def main():
	args = ospray.ospInit(sys.argv)
	print(repr(args))
	
	print("Setting up scene camera...")
	
	cam = ospray.ospNewCamera("perspective")
	ospray.set(cam, "aspect", OSP_FLOAT, 3840.0/2160.0)
	ospray.set(cam, "pos", OSP_FLOAT3, (0.0, 0.0, 0.0))
	ospray.set(cam, "dir", OSP_FLOAT3, (0.1, 0.0, 1.0))
	ospray.set(cam, "up", OSP_FLOAT3, (0.0, 1.0, 0.0))
	ospray.commit(cam)
	
	print("Creating geometries...")
	
	trimesh = ospray.ospNewGeometry("triangles")
	
	vertices = [
		-1.00, -1.00, 3.00, 0.0,
        -1.00,  1.00, 3.00, 0.0,
         1.00, -1.00, 3.00, 0.0,
         0.10,  0.10, 0.30, 0.0 
	]
	
	data = ospray.ospNewData(OSP_FLOAT3A, vertices)
	ospray.commit(data)
	ospray.set(trimesh, "vertex", OSP_DATA, data)
	ospray.release(data)
	
	colors = [
		0.9, 0.5, 0.5, 1.0,
		0.8, 0.8, 0.8, 1.0,
		0.8, 0.8, 0.8, 1.0,
        0.5, 0.9, 0.5, 1.0
	]
	
	data = ospray.ospNewData(OSP_FLOAT4, colors)
	ospray.commit(data)
	ospray.set(trimesh, "vertex.color", OSP_DATA, data)
	ospray.release(data)
	
	indices = [
		0, 1, 2,
		1, 2, 3
	]
	
	data = ospray.ospNewData(OSP_INT3, indices)
	ospray.commit(data)
	ospray.set(trimesh, "index", OSP_DATA, data)
	ospray.release(data)
	
	ospray.commit(trimesh)
	
	print("Creating world model...")
	
	world = ospray.ospNewModel()
	ospray.ospAddGeometry(world, trimesh)
	ospray.release(trimesh)
	ospray.commit(world)
	
	
	print("Setting up renderer...")
	
	renderer = ospray.ospNewRenderer("scivis")
	light = ospray.ospNewLight("ambient")
	ospray.commit(light)
	
	print("Setting up light data...")
	
	lightdata = ospray.ospNewData(OSP_LIGHT, [light])
	ospray.commit(lightdata)
	
	
	ospray.set(renderer, "aoSamples", OSP_INT, 1)
	ospray.set(renderer, "bgColor", OSP_FLOAT, 1.0)
	ospray.set(renderer, "model", OSP_MODEL, world)
	ospray.set(renderer, "camera", OSP_CAMERA, cam)
	ospray.set(renderer, "lights", OSP_DATA, lightdata)
	ospray.commit(renderer)
	
	# These are no longer needed
	ospray.release(light)
	ospray.release(cam)
	ospray.release(lightdata)
	ospray.release(world)
	
	print("Setting up framebuffer...")
	
	framebuffer = ospray.ospNewFrameBuffer((3840, 2160), OSP_FB_RGBA8, OSP_FB_COLOR | OSP_FB_ACCUM)
	
	print("Cleaning framebuffer...")
	
	ospray.ospFrameBufferClear(framebuffer, OSP_FB_COLOR | OSP_FB_ACCUM)
	
	print("Rendering initial frame...")
	ospray.ospRenderFrame(framebuffer, renderer, OSP_FB_COLOR | OSP_FB_ACCUM)
	
	
	# Make a few more passes to refine the image
	print("Refining image...")
	for i in range(0, 10):
		ospray.ospRenderFrame(framebuffer, renderer, OSP_FB_COLOR | OSP_FB_ACCUM)
	
	
	print("Writing framebuffer to file...")
	fbdata = ospray.ospMapFrameBuffer(framebuffer, OSP_FB_COLOR)
	ospray.utils.writePPM("triangles.ppm", (3840, 2160), fbdata)
	
	ospray.ospUnmapFrameBuffer(fbdata, framebuffer)
	
	print("Cleaning up...")
	ospray.release(renderer)
	ospray.release(framebuffer)
	
	ospray.ospShutdown()
	return 0


if __name__ == '__main__':
	main()
