
__all__ = ["constants", "utils"]

from ctypes import *
from ctypes import util
import sys
from ospray.constants.types import *
from ospray.constants.formats import *
from ospray.constants.channels import *

class VEC2I(Structure):
	_fields_ = [
		("x", c_int),
		("y", c_int)
	]

# Alias for printing to stderr
def perror(message):
	print(message, file=sys.stderr)

OSPRAY_ISPC = CDLL(util.find_library("ospray_module_ispc"), mode=RTLD_GLOBAL)
OSPRAY = cdll.LoadLibrary(util.find_library("ospray"))
perror("Loaded {0}".format(OSPRAY))

# API bindings

def ospInit(argv):
	char_p_p = POINTER(POINTER(c_char))
	c_ospInit = OSPRAY.ospInit
	c_ospInit.restype = c_uint32
	c_ospInit.argtypes = (POINTER(c_int), char_p_p)
	argc = c_int(len(argv))
	c_argv = (POINTER(c_char) * (len(argv) + 1))()
	for i, arg in enumerate(argv):
		c_argv[i] = create_string_buffer(arg.encode('utf-8'))
	
	err = c_ospInit(argc, c_argv)
	
	remaining_args = []
	for i in range(0, argc.value):
		remaining_args.append(cast(c_argv[i], c_char_p).value.decode("utf-8"))
	
	return {"err": err, "argv": remaining_args}


def ospShutdown():
	c_ospShutdown = OSPRAY.ospShutdown
	c_ospShutdown()


def ospLoadModule(type):
	c_ospLoadModule = OSPRAY.ospLoadModule
	c_type = create_string_buffer(type.encode('utf-8'))
	c_ospLoadModule()

	
def ospCommit(obj):
	c_ospCommit = OSPRAY.ospCommit
	c_ospCommit(c_void_p(obj))

# Alias without osp* prefix
def commit(obj):
	ospCommit(obj)


def ospRelease(obj):
	c_ospRelease = OSPRAY.ospRelease
	c_ospRelease(c_void_p(obj))

# Alias without osp* prefix
def release(obj):
	ospRelease(obj)


# Setter bindings

def set(obj, id, type, value):
	
	c_id = create_string_buffer(id.encode('utf-8'))
	
	def setString():
		c_ospSetString = OSPRAY.ospSetString
		c_val = create_string_buffer(value.encode('utf-8'))
		c_ospSetString(c_void_p(obj), c_id, c_val)
	
	def setObject():
		c_ospSetObject = OSPRAY.ospSetObject
		c_ospSetObject(c_void_p(obj), c_id, c_void_p(value))
		
	def setVoidPtr():
		c_ospSetVoidPtr = OSPRAY.ospSetVoidPtr
		c_ospSetVoidPtr(c_void_p(obj), c_id, c_void_p(value))
		
	def set1f():
		c_ospSetf = OSPRAY.ospSetf
		c_ospSetf(c_void_p(obj), c_id, c_float(value))
	
	def set2f():
		c_ospSet2f = OSPRAY.ospSet2f
		c_ospSet2f(c_void_p(obj), c_id, c_float(value[0]), c_float(value[1]))
		
	def set3f():
		c_ospSet3f = OSPRAY.ospSet3f
		c_ospSet3f(c_void_p(obj), c_id, c_float(value[0]), c_float(value[1]), c_float(value[2]))
		
	def set4f():
		c_ospSet4f = OSPRAY.ospSet4f
		c_ospSet4f(c_void_p(obj), c_id, c_float(value[0]), c_float(value[1]), c_float(value[2]), c_float(value[3]))
	
	def set1i():
		c_ospSeti = OSPRAY.ospSeti
		c_ospSeti(c_void_p(obj), c_id, c_int(value))
	
	def set2i():
		c_ospSet2i = OSPRAY.ospSet2i
		c_ospSet2i(c_void_p(obj), c_id, c_int(value[0]), c_int(value[1]))
		
	def set3i():
		c_ospSet3i = OSPRAY.ospSet3i
		c_ospSet3i(c_void_p(obj), c_id, c_int(value[0]), c_int(value[1]), c_int(value[2]))
		
	def setData():
		c_ospSetData = OSPRAY.ospSetData
		c_ospSetData(c_void_p(obj), c_id, c_void_p(value))
	
	def setUnknown():
		raise TypeError("Unsupported OSPRay type!")
		
	switch = {
		OSP_DEVICE				: setUnknown,
		OSP_VOID_PTR			: setVoidPtr,
		OSP_OBJECT				: setObject,
		OSP_CAMERA				: setObject,
		OSP_DATA				: setObject,
		OSP_FRAMEBUFFER			: setObject,
		OSP_GEOMETRY			: setObject,
		OSP_LIGHT				: setObject,
		OSP_MATERIAL			: setObject,
		OSP_MODEL				: setObject,
		OSP_RENDERER			: setObject,
		OSP_TEXTURE				: setObject,
		OSP_TRANSFER_FUNCTION	: setObject,
		OSP_VOLUME				: setObject,
		OSP_PIXEL_OP			: setObject,
		OSP_STRING				: setString,
		OSP_CHAR				: set1i,
		OSP_UCHAR				: set1i,
		OSP_UCHAR2				: set2i,
		OSP_UCHAR3				: set3i,
		OSP_UCHAR4				: setUnknown,
		OSP_SHORT				: set1i,
		OSP_USHORT				: set1i,
		OSP_INT					: set1i,
		OSP_INT2				: set2i,
		OSP_INT3				: set3i,
		OSP_INT4				: setUnknown,
		OSP_UINT				: set1i,
		OSP_UINT2				: set2i,
		OSP_UINT3				: set3i,
		OSP_UINT4				: setUnknown,
		OSP_LONG				: setUnknown,
		OSP_LONG2				: setUnknown,
		OSP_LONG3				: setUnknown,
		OSP_LONG4				: setUnknown,
		OSP_ULONG				: setUnknown,
		OSP_ULONG2				: setUnknown,
		OSP_ULONG3				: setUnknown,
		OSP_ULONG4				: setUnknown,
		OSP_FLOAT				: set1f,
		OSP_FLOAT2				: set2f,
		OSP_FLOAT3				: set3f,
		OSP_FLOAT4				: set4f,
		OSP_FLOAT3A				: set4f,
		OSP_DOUBLE				: setUnknown,
		OSP_UNKNOWN				: setUnknown
	}
	switch[type]()


def ospNewData(type, source):
	
	
	# void* objects will already by ctyped will not require conversion
	def unpackVoidPtr():
		try:
			len(source)
		except TypeError as e:
			raise TypeError("Source argument for OSP_VOID_PTR must be list!")
		
		return (c_size_t(len(source[0])), source[0])
	
	def unpackObject():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_void_p(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_void_p * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
		
		return (num, c_source)
		
	def unpackString():
		num = c_size_t(len(source))
		c_source = create_string_buffer(source.encode('utf-8'))
		return (num, c_source)
		
	def unpackChar():
		c_source = c_char(source)
		return (c_size_t(1), c_source)
		
	def unpackUChar():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_ubyte(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_ubyte * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
		
	def unpackShort():
		c_source = c_int16(source)
		return (c_size_t(1), c_source)
		
	def unpackUShort():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_uint16(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_uint16 * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
	
	def unpackInt():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_int32(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_int32 * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
		
	def unpackUInt():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_uint32(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_uint32 * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
	
	def unpackLong():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_int64(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_int64 * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
	
	def unpackULong():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_uint64(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_uint64 * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
	
	def unpackFloat():
		try:
			len(source)
		except TypeError as e:
			num = c_size_t(1)
			c_source = c_float(source)
		else:
			num = c_size_t(len(source))
			c_source = (c_float * len(source))()
			for i, arg in enumerate(source):
				c_source[i] = arg
			
		return (num, c_source)
	
	def unpackDouble():
		c_source = c_double(source)
		return (c_size_t(1), c_source)
		
	def unpackUnknown():
		raise TypeError("Unsupported OSPRay type!")
	
	switch = {
		OSP_DEVICE				: unpackUnknown,
		OSP_VOID_PTR			: unpackVoidPtr,
		OSP_OBJECT				: unpackObject,
		OSP_CAMERA				: unpackObject,
		OSP_DATA				: unpackObject,
		OSP_FRAMEBUFFER			: unpackObject,
		OSP_GEOMETRY			: unpackObject,
		OSP_LIGHT				: unpackObject,
		OSP_MATERIAL			: unpackObject,
		OSP_MODEL				: unpackObject,
		OSP_RENDERER			: unpackObject,
		OSP_TEXTURE				: unpackObject,
		OSP_TRANSFER_FUNCTION	: unpackObject,
		OSP_VOLUME				: unpackObject,
		OSP_PIXEL_OP			: unpackObject,
		OSP_STRING				: unpackString,
		OSP_CHAR				: unpackChar,
		OSP_UCHAR				: unpackUChar,
		OSP_UCHAR2				: unpackUChar,
		OSP_UCHAR3				: unpackUChar,
		OSP_UCHAR4				: unpackUChar,
		OSP_SHORT				: unpackShort,
		OSP_USHORT				: unpackUShort,
		OSP_INT					: unpackInt,
		OSP_INT2				: unpackInt,
		OSP_INT3				: unpackInt,
		OSP_INT4				: unpackInt,
		OSP_UINT				: unpackUInt,
		OSP_UINT2				: unpackUInt,
		OSP_UINT3				: unpackUInt,
		OSP_UINT4				: unpackUInt,
		OSP_LONG				: unpackLong,
		OSP_LONG2				: unpackLong,
		OSP_LONG3				: unpackLong,
		OSP_LONG4				: unpackLong,
		OSP_ULONG				: unpackULong,
		OSP_ULONG2				: unpackULong,
		OSP_ULONG3				: unpackULong,
		OSP_ULONG4				: unpackULong,
		OSP_FLOAT				: unpackFloat,
		OSP_FLOAT2				: unpackFloat,
		OSP_FLOAT3				: unpackFloat,
		OSP_FLOAT4				: unpackFloat,
		OSP_FLOAT3A				: unpackFloat,
		OSP_DOUBLE				: unpackDouble,
		OSP_UNKNOWN				: unpackUnknown
	}
	
	
	c_ospNewData = OSPRAY.ospNewData
	c_ospNewData.restype = c_void_p
	c_num, c_src = switch[type]()
	c_type = c_uint32(type)
	return c_ospNewData(c_num, c_type, c_src)


def ospNewVolume(type):
	c_ospNewVolume = OSPRAY.ospNewVolume
	c_ospNewVolume.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewVolume(c_type);
	

def ospNewGeometry(type):
	c_ospNewGeometry = OSPRAY.ospNewGeometry
	c_ospNewGeometry.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewGeometry(c_type)
	

# TODO: Add this once transforms are implemented
def ospNewInstance(model, transform):
	raise NotImplementedError()
	

def ospNewRenderer(type):
	c_ospNewRenderer = OSPRAY.ospNewRenderer
	c_ospNewRenderer.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewRenderer(c_type)


def ospNewModel():
	c_ospNewModel = OSPRAY.ospNewModel
	c_ospNewModel.restype = c_void_p
	return c_ospNewModel()


def ospAddGeometry(model, geo):
	c_ospAddGeometry = OSPRAY.ospAddGeometry
	c_ospAddGeometry(c_void_p(model), c_void_p(geo))


def ospAddVolume(model, vol):
	c_ospAddVolume = OSPRAY.ospAddVolume
	c_ospAddVolume(c_void_p(model), c_void_p(vol))
	

def ospRemoveGeometry(model, geo):
	c_ospRemoveGeometry = OSPRAY.ospRemoveGeometry
	c_ospRemoveGeometry(c_void_p(model), c_void_p(geo))
	

def ospRemoveVolume(model, geo):
	c_ospRemoveVolume = OSPRAY.ospRemoveVolume
	c_ospRemoveVolume(c_void_p(model), c_void_p(geo))


def ospNewLight(type):
	c_ospNewLight = OSPRAY.ospNewLight3
	c_ospNewLight.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewLight(c_type)
	

def ospNewMaterial(render_type, mat_type):
	c_ospNewMaterial = OSPRAY.ospNewMaterial2
	c_ospNewMaterial.restype = c_void_p
	c_render_type = create_string_buffer(render_type.encode('utf-8'))
	c_mat_type = create_string_buffer(mat_type.encode('utf-8'))
	return c_ospNewMaterial(c_render_type, c_mat_type)
	

def ospSetMaterial(geo, mat):
	c_ospSetMaterial = OSPRAY.ospSetMaterial
	c_ospSetMaterial(geo, mat)
	

def ospNewTexture(type):
	c_ospNewTexture = OSPRAY.ospNewTexture
	c_ospNewTexture.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewTexture(c_type)
	

def ospNewCamera(type):
	c_ospNewCamera = OSPRAY.ospNewCamera
	c_ospNewCamera.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewCamera(c_type)
	

# This requires a return struct to be implemented
def ospPick(result, renderer, pos):
	raise NotImplementedError() 


def ospNewFrameBuffer(size, format, channels):
	c_ospNewFrameBuffer = OSPRAY.ospNewFrameBuffer
	c_ospNewFrameBuffer.restype = c_void_p
	c_ospNewFrameBuffer.argtypes = (POINTER(VEC2I), c_uint32, c_uint32)
	c_size = VEC2I(size[0], size[1])
	c_format = c_uint32(format)
	c_channels = c_uint32(channels)
	# print("Calling C func with args:")
	# print("\t" + repr(c_size))
	# print("\t" + repr(c_format))
	# print("\t" + repr(c_channels))
	return c_ospNewFrameBuffer(byref(c_size), c_format, c_channels)


def ospMapFrameBuffer(fb, channels = OSP_FB_COLOR):
	c_ospMapFrameBuffer = OSPRAY.ospMapFrameBuffer
	c_ospMapFrameBuffer.restype = POINTER(c_char)
	c_channels = c_uint32(channels)
	return c_ospMapFrameBuffer(c_void_p(fb), c_channels)


def ospUnmapFrameBuffer(mapped, fb):
	c_ospUnmapFrameBuffer = OSPRAY.ospUnmapFrameBuffer
	c_ospUnmapFrameBuffer.argtypes = (POINTER(c_char), c_void_p)
	c_ospUnmapFrameBuffer(mapped, c_void_p(fb))


def ospFrameBufferClear(fb, channels):
	c_ospFrameBufferClear = OSPRAY.ospFrameBufferClear
	c_channels = c_uint32(channels)
	c_ospFrameBufferClear(c_void_p(fb), c_channels)
	

def ospNewPixelOp(type):
	c_ospNewPixelOp = OSPRAY.ospNewPixelOp
	c_ospNewPixelOp.restype = c_void_p
	c_type = create_string_buffer(type.encode('utf-8'))
	return c_ospNewPixelOp(c_type)
	

def ospSetPixelOp(fb, pixop):
	c_ospSetPixelOp = OSPRAY.ospSetPixelOp
	c_ospSetPixelOp(fb, pixop)


def ospRenderFrame(fb, renderer, channels = OSP_FB_COLOR):
	c_ospRenderFrame = OSPRAY.ospRenderFrame
	c_ospRenderFrame.restype = c_float
	c_channels = c_uint32(channels)
	return c_ospRenderFrame(c_void_p(fb), c_void_p(renderer), c_channels)


def ospSetProgressFunc(func, uptr):
	raise NotImplementedError()



