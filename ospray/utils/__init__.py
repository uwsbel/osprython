
from ctypes import *

def writePPM(filename, screensize, pixelbuffer):
	# Buffer length is the screen dimensions times the number of bytes per pixel
	buflen = screensize[0] * screensize[1] * 4
	
	with open(filename, "wb+") as f:
		# Write PPM file header
		
		f.write("P6\n".encode('utf-8'))
		dimension_str = str(screensize[0]) + " " + str(screensize[1]) + "\n"
		f.write(dimension_str.encode('utf-8'))
		f.write("255\n".encode('utf-8'))
		
		for y in range(0, screensize[1]):
			for x in range(0, screensize[0]):
				# line_num * (stride * bpp) + (col_num * bpp)
				base_offset = y * (screensize[0] * 4) + (x * 4)
				f.write(pixelbuffer[base_offset + 0])
				f.write(pixelbuffer[base_offset + 1])
				f.write(pixelbuffer[base_offset + 2])
				# the ppm format skips the alpha channel
				# f.write(pixelbuffer[base_offset + 3])
				
		f.write("\n".encode('utf-8'))
				


